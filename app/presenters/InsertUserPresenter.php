<?php

/**
 * Soubor s presenterem pro akci "vložení nového uživatele".
 * Data k novému uživateli aplikace přijímá jako POST data.
 *
 * @package  OXY - sluzba
 * @subpackage  Presenters
 * @author   Martin Marek <martin.marek@impnet.cz>
 * @version  $Revision: 1.0 $
 */

namespace App\Presenters;

use Nette,
    App\Model,
    Tracy\ILogger,
    Tracy\Debugger,
    Nette\Application\UI\Form,
    Nette\Http\Response,
    Nette\Http\IResponse;    

/**
 * Presenter pro vložení nového uživatele.
 * Data k novému uživateli aplikace přijímá jako POST data.
 *
 * @package  OXY - sluzba
 * @author   Martin Marek <martin.marek@impnet.cz>
 * @version  $Revision: 1.0 $
 */
final class InsertuserPresenter extends Nette\Application\UI\Presenter
{
  /** @var Model\Registration */
  private $registration;

  public function getRegistration()
  {
    return $this->registration;
  }

  //
  public function __construct( Model\Registration $registration )
  {
    parent::__construct();

    $this->registration = $registration;
  }

  public function startup()
  {   
    parent::startup();

    $secureKey = $this->getParameter( 'securekey' );

    $httpResponse = $this->getHttpResponse();

    if ( $secureKey != 'x5yW78q9' )
    {
      $this->error( NULL, IResponse::S401_UNAUTHORIZED );
    }

  }

  public function beforeRender()
  {
    parent::beforeRender();
  }

  public function actionDefault()
  {
    $result = 'Uživatel nebyl zaregistrován.';
    $postRequest = $this->getHttpRequest()->getPost();

/*
    // Testovaci POST data (bezny uživatel)
    $postRequest = array(
      'name'     => 'Josef Novak',
      'password' => '123456897',
      'email'    => 'josef.novak@email.com',
      'role'     => 0,
    );
*/

/*
    // Testovaci POST data (uživatel s roli administrator)
    $postRequest = array(
      'name'     => 'Josef Novak',
      'password' => '123456897',
      'email'    => 'josef.novak@email.com',
      'role'     => 1,
    );
*/

    // Debugger::log( $postRequest );

    if ( !empty( $postRequest ) )
    {
      if ( !empty( $postRequest['name'] ) && !empty( $postRequest['password'] ) && !empty( $postRequest['email'] ) )
      {

        // Debugger::log( $postRequest );
        $this->getRegistration()->insertUser( $postRequest );
      }

      $result = 'Uživatel byl úspěšně zaregistrován.';
    }

    $this->template->result = $result;  
  }

}
