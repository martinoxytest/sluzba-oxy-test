<?php

/**
 * Soubor s presenterem pro akci "vygenerovani souboru xml s seznamem uzivatelu".
 *
 * @package  OXY - sluzba
 * @subpackage  Presenters
 * @author   Martin Marek <martin.marek@impnet.cz>
 * @version  $Revision: 1.0 $
 */

namespace App\Presenters;

use Nette,
    App\Model,
    Tracy\ILogger,
    Tracy\Debugger,
    Nette\Application\UI\Form,
    Nette\Http\Response,
    Nette\Http\IResponse;    

/**
 * Presenterem pro vygenerovani souboru xml s seznamem uzivatelu.
 *
 * @package  OXY - sluzba
 * @author   Martin Marek <martin.marek@impnet.cz>
 * @version  $Revision: 1.0 $
 */
final class UserslistPresenter extends Nette\Application\UI\Presenter
{
  /** @var Model\Registration */
  private $registration;

  public function getRegistration()
  {
    return $this->registration;
  }

  //
  public function __construct( Model\Registration $registration )
  {
    parent::__construct();

    $this->registration = $registration;
  }

  public function startup()
  {   
    parent::startup();

    $secureKey = $this->getParameter( 'securekey' );

    $httpResponse = $this->getHttpResponse();

    if ( $secureKey != 'x5yW78q9' )
    {
      $this->error(NULL, IResponse::S401_UNAUTHORIZED);
    }

  }

  public function beforeRender()
  {
    parent::beforeRender();

    // nastaveni typu hlavicky na xml...
    $this->getHttpResponse()->setContentType( 'application/xml' );
  }

  public function actionDefault()
  {
    $this->template->usersList           = true;
    $this->template->registeredUsersList = $this->getRegistration()->findAll();
  }

}
