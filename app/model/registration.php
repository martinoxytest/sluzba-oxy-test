<?php

/**
 * Soubor s modelovymi tridami pro sluzbu "Uzivatelsky system".
 * Sluzba slouzi jako testovaci ukol pro PHP programatora
 * ve spolecnosti OXY.
 *
 * @package  OXY - sluzba
 * @subpackage  Model
 * @author   Martin Marek <martin.marek@impnet.cz>
 * @version  $Revision: 1.0 $
 */


namespace App\Model;

use Nette,
    Exception,
    Nette\Database\SqlLiteral,
    Tracy\ILogger,
    Tracy\Debugger;

/**
 * Zaznamova trida pro model Registration.
 * Wrapper nad Nette\Database NotORM
 *
 * @package  CMS
 * @author   Martin Marek <martin.marek@impnet.cz>
 * @version  $Revision: 1.0 $
 */
class UserRecord
{

  use Nette\SmartObject;

  /** @var Nette\Database\Table\ActiveRow */
  private $data;

  /** @var string[] */
  private $tableData;

  /** @var string */
  private $table;

  /** @var ServiceHelper */
  private $serviceHelper;

  /**
   * @param Nette\Database\Table\ActiveRow $data
   * @param string[] $tableData
   * @param string $table
   * @return self
   */
  public function __construct( Nette\Database\Table\ActiveRow $data, $tableData,
                               $table )
  {
    $this->data      = $data;
    $this->tableData = $tableData;
    $this->table     = $table;
  }

  /**
   * @param ServiceHelper $serviceHelper
   * @return void
   */
  public function setServiceHelper( ServiceHelper $serviceHelper )
  {
    $this->serviceHelper = $serviceHelper;
  }

  /**
   * @return ServiceHelper */
  public function getServiceHelper()
  {
    return $this->serviceHelper;
  }

  /**
   * @return Nette\Database\Table\ActiveRow */
  public function data()
  {
    return $this->data;
  }

  /**
   * @param string $key
   * @return string */
  public function tableData( $key )
  {
    return $this->tableData[$key];
  }

  /**
   * @return string */
  public function table()
  {
    return $this->table;
  }

  /**
   * @return array */
  public function getAllTableData()
  {
    return $this->tableData;
  }
}

/**
 * Modelova trida pro registraci/spravu uzivatelu.
 *
 * @package  OXY - sluzba
 * @author   Martin Marek <martin.marek@impnet.cz>
 * @version  $Revision: 1.0 $
 */
class Registration
{

  use Nette\SmartObject;

  /** @var Nette\Database\Context*/
  private $database;

  /** @var ServiceHelper */
  private $serviceHelper;

  /** @const TABLE_ITEM */
  const TABLE_USER_NAME = 'users';

  /** @var string[] */
  public $tableData = array(
      'TABLE_USER_NAME' => self::TABLE_USER_NAME,
  );

  /**
   * @param Nette\Database\Context $database
   * @return self
   */
  public function __construct( Nette\Database\Context $database )
  {
    $this->database  = $database;
  }

  /**
   * @return Nette\Database\Context
   */
  public function getDatabase()
  {
    return $this->database;
  }

  /**
   * @return array */
  public function getAllTableData()
  {
    return $this->tableData;
  }

  /**
   * @param ServiceHelper $serviceHelper
   * @return void
   */
  public function setServiceHelper( ServiceHelper $serviceHelper )
  {
    $this->serviceHelper = $serviceHelper;
  }

  /**
   * @return ServiceHelper */
  public function getServiceHelper()
  {
    return $this->serviceHelper;
  }

  /**
   * @param mixed $where
   * @param string $order
   * @param int $limit
   * @param int $offset
   * @param string $group
   * @return UserRecord[]
   */
  public function findAll( $where = NULL, $order = NULL, $limit = NULL,
                           $offset = NULL, $group = NULL )
  {
    $result = array();

    $data = $this->getDatabase()->table( self::TABLE_USER_NAME );

    if ( isset( $where ) )
      $data->where( $where );

    if ( isset( $order ) )
      $data->order( $order );

    if ( isset( $group ) )
      $data->group( $group );

    if ( isset( $limit ) )
      $data->limit( $limit, $offset );

    foreach ( $data as $d )
    {
      $result[]   = new UserRecord( $d, $this->tableData, self::TABLE_USER_NAME );
    }

    return $result;
  }

  /**
   * @param mixed[] $values
   * @return Nette\Database\Table\ActiveRow|false
   */
  public function insertUser( $values )
  {
    $password = trim( $values['password'] );
    $password = Nette\Security\Passwords::hash( $password );
  
    $values['password'] = $password;
    $values[ 'email' ]  = trim( $values['email'] );
    $values[ 'name' ]   = trim( $values['name'] );

    if ( isset( $values[ 'role' ] ) && $values[ 'role' ] != 1 )
      unset( $values[ 'role' ] );

    unset( $values['passwordAgain'] );

    $row = $this->getDatabase()->table( self::TABLE_USER_NAME )->insert( $values );

    if ( $row === false )
      throw new Exception( "Nepodařilo se vložit záznam." );

    return $row;
  }

}
