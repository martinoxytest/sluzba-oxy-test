-- Adminer 4.2.4 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP DATABASE IF EXISTS `oxy-test`;
CREATE DATABASE `oxy-test` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `oxy-test`;

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 = user, 1 = admin ',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `users` (`id`, `email`, `name`, `password`, `role`) VALUES
(1,	'test@users.com',	'Test Uživatel I.',	'$2y$10$VaZOCqAx8tx2cvADlP4rlu1RxiAD8rdvEFoquJac2LSub0wMHb4jO',	0),
(2,	'test@admins.com',	'Test Administrátor I.',	'$2y$10$VaZOCqAx8tx2cvADlP4rlu1RxiAD8rdvEFoquJac2LSub0wMHb4jO',	1),
(3,	'test2@users.com',	'Test Uživatel II.',	'$2y$10$VaZOCqAx8tx2cvADlP4rlu1RxiAD8rdvEFoquJac2LSub0wMHb4jO',	0),
(4,	'test2@admins.com',	'Test Administrátor II.',	'$2y$10$qI4vVj.pdcZ0v8WkqqGRieBPHTUGGLs/6YfVqDd0Z7hMDy01Z8/iy',	1),
(5,	'test3@admins.com',	'Test Administrátor III.',	'$2y$10$RUrXEo2liGllox0d36GbLO53VSISkDF.JzVkGyvSFOlbCdp.sZs2C',	1),
(15,	'test3@users.com',	'Test Uživatel III.',	'$2y$10$GbDMoBYTfmw8eiC2YG5QtO5oBSVN8.k20WmattyUrtSgK3UvdbCnO',	0),
(16,	'test4@admins.com',	'Test Administrátor IV.',	'$2y$10$eZWLktWgXAl7gu36eINzCOSDP/ENrYD8DME/5atm3AHIpLXz99HdO',	1);

-- 2018-11-25 16:06:25
