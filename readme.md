Oxy - test uloha
=================

Navod pro spusteni testovaci ulohy
-----------------------------------
Prostředí, ve kterém jsem aplikaci vytvořil:
- PHP 7.1
- MySQL 5.0.12
- Apache 2.4.18

- jako OS používám Ubuntu 16.04LTS (úkol by měl fungovat na "libovolném" OS)

Testovací úkol jsem vypracoval jako dvě webové aplikace vytvořené pomocí frameworku Nette (aplikace "Sluzba" a aplikace "Formular").

Pro kontrolu nastavení prostředí, ve kterém poběží Nette Framework lze použít http://www.sluzba.oxy.lcz/requirements-checker/ .

Export databáze je v adresáři db v souboru users.sql (tento soubor stačí naimportovat do mysql, např. pomocí admineru). Přístupy k databázi (dbname, user, password) se v Nette nastavují v souboru www.sluzba.oxy.lcz/app/config/config.local.neon, případně v souboru www.sluzba.oxy.lcz/app/config/config.neon .

První aplikace "Sluzba" implementuje webovou službu pro registraci uživatelů  (vložení/registrace uživatele, vrácení seznamu uživatelů). "Služba" je vytvořena jako varianta SOAP API. Údaje registrovaných uživatelů jsou dostupné jako XML export/soubor na adrese http://www.sluzba.oxy.lcz/userslist?securekey=x5yW78q9 . Data nově registrovaného uživatele služba přijímá jako POST data na adrese http://www.sluzba.oxy.lcz/insertuser?securekey=x5yW78q9 .
Pro práci s touto aplikací jsem si ve svém prostředí/počítači/na localhostu vytvořil apache virtual host. U mě na Ubuntu je to níže uvedný zaznam v souboru /etc/apache2/sites-available/www.sluzba.oxy.lcz.conf :
<VirtualHost *:80>
        # The ServerName directive sets the request scheme, hostname and port that
        # the server uses to identify itself. This is used when creating
        # redirection URLs. In the context of virtual hosts, the ServerName
        # specifies what hostname must appear in the request's Host: header to
        # match this virtual host. For the default virtual host (this file) this
        # value is not decisive as it is used as a last resort host regardless.
        # However, you must set it for any further virtual host explicitly.
        #ServerName www.example.com

        #ServerAdmin webmaster@localhost
        DocumentRoot /var/www/html/www.sluzba.oxy.lcz/www
        # /web/public/www/
        ServerName www.sluzba.oxy.lcz

        # Available loglevels: trace8, ..., trace1, debug, info, notice, warn,
        # error, crit, alert, emerg.
        # It is also possible to configure the loglevel for particular
        # modules, e.g.
        #LogLevel info ssl:warn

        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined

        # For most configuration files from conf-available/, which are
        # enabled or disabled at a global level, it is possible to
        # include a line for only one particular virtual host. For example the
        # following line enables the CGI configuration for this host only
        # after it has been globally disabled with "a2disconf".
        #Include conf-available/serve-cgi-bin.conf
</VirtualHost>

Na ubuntu se virtual host poté spustí takto:
a2ensite www.sluzba.oxy.lcz.conf
apache service restart

Také jsem přesměroval doménu www.sluzba.oxy.lcz na localhost. V Ubuntu níže uvedený řádek v souboru /etc/hosts :
127.0.0.1       www.sluzba.oxy.lcz

Pro práci s aplikací "Sluzba" je možné použít aplikaci "Formular". Pro tu jsem si vytvořil také virtual host:

U mě na Ubuntu je to níže uvedný zaznam v souboru /etc/apache2/sites-available/www.formular.oxy.lcz.conf :
<VirtualHost *:80>
        # The ServerName directive sets the request scheme, hostname and port that
        # the server uses to identify itself. This is used when creating
        # redirection URLs. In the context of virtual hosts, the ServerName
        # specifies what hostname must appear in the request's Host: header to
        # match this virtual host. For the default virtual host (this file) this
        # value is not decisive as it is used as a last resort host regardless.
        # However, you must set it for any further virtual host explicitly.
        #ServerName www.example.com

        #ServerAdmin webmaster@localhost
        DocumentRoot /var/www/html/www.formular.oxy.lcz/www
        # /web/public/www/
        ServerName www.formular.oxy.lcz

        # Available loglevels: trace8, ..., trace1, debug, info, notice, warn,
        # error, crit, alert, emerg.
        # It is also possible to configure the loglevel for particular
        # modules, e.g.
        #LogLevel info ssl:warn

        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined

        # For most configuration files from conf-available/, which are
        # enabled or disabled at a global level, it is possible to
        # include a line for only one particular virtual host. For example the
        # following line enables the CGI configuration for this host only
        # after it has been globally disabled with "a2disconf".
        #Include conf-available/serve-cgi-bin.conf
</VirtualHost>

Na ubuntu se virtual host poté spustí takto:
a2ensite www.formular.oxy.lcz.conf
apache service restart

Také jsem přesměroval doménu www.formular.oxy.lcz na localhost. V Ubuntu níže uvedený řádek v souboru /etc/hosts :
127.0.0.1       www.formular.oxy.lcz





Nette Web Project
=================

This is a simple, skeleton application using the [Nette](https://nette.org). This is meant to
be used as a starting point for your new projects.

[Nette](https://nette.org) is a popular tool for PHP web development.
It is designed to be the most usable and friendliest as possible. It focuses
on security and performance and is definitely one of the safest PHP frameworks.

If you like Nette, **[please make a donation now](https://nette.org/donate)**. Thank you!


Requirements
------------

PHP 5.6 or higher.


Installation
------------

The best way to install Web Project is using Composer. If you don't have Composer yet,
download it following [the instructions](https://doc.nette.org/composer). Then use command:

	composer create-project nette/web-project path/to/install
	cd path/to/install


Make directories `temp/` and `log/` writable.


Web Server Setup
----------------

The simplest way to get started is to start the built-in PHP server in the root directory of your project:

	php -S localhost:8000 -t www

Then visit `http://localhost:8000` in your browser to see the welcome page.

For Apache or Nginx, setup a virtual host to point to the `www/` directory of the project and you
should be ready to go.

**It is CRITICAL that whole `app/`, `log/` and `temp/` directories are not accessible directly
via a web browser. See [security warning](https://nette.org/security-warning).**

Notice: Composer PHP version
----------------------------
This project forces `PHP 5.6` as your PHP version for Composer packages. If you have newer version on production you should change it in `composer.json`.
```json
"config": {
	"platform": {
		"php": "7.0"
	}
}
```
